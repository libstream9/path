#include <stream9/path/home_directory.hpp>

#include <stdlib.h>

#include <stream9/strings/empty.hpp>

namespace stream9::path {

string
home_directory()
{
    auto* const env = ::secure_getenv("HOME");
    if (!env || str::empty(env)) {
        throw error {
            errc::invalid_environment_variable, {
                { "name", "HOME" },
                { "value", "" },
                { "description", "HOME isn't set or empty."}
            }
        };
    }
    else if (env[0] != '/') {
        throw error {
            errc::invalid_environment_variable, {
                { "name", "HOME" },
                { "value", env },
                { "description", "The path isn't absolute."}
            }
        };
    }

    return env;
}

} // namespace stream9::path
