#include <stream9/path/error.hpp>

namespace stream9::path {

std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept { return "stream9::path"; }

        std::string message(int ec) const
        {
            switch (static_cast<errc>(ec)) {
                using enum errc;
                case ok:
                    return "ok";
                case directory_does_not_exist:
                    return "directory_does_not_exist";
                case invalid_environment_variable:
                    return "invalid environment variable";
            }
            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::path
