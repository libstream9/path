#include <stream9/path/normalize.hpp>

#include <stream9/path/absolute.hpp>
#include <stream9/path/components.hpp>
#include <stream9/path/concat.hpp> // operator/

#include <stream9/array.hpp>
#include <stream9/push_back.hpp>
#include <stream9/pop_back.hpp>
#include <stream9/strings/back.hpp>

namespace stream9::path {

static string
join(array<string_view> const& comps, bool has_trailing_slash)
{
    string s;

    if (empty(comps)) {
        s = ".";
    }
    else {
        for (auto const& c: comps) {
            s /= c;
        }
    }

    if (has_trailing_slash) {
        s += "/";
    }

    return s;
}

string
normalize(string_view p, bool preserve_trailing_slash/*= false*/)
{
    try {
        if (p.empty()) return string(p);

        bool has_trailing_slash = preserve_trailing_slash
                               && p != "/"
                               && str::back(p) == '/';

        array<string_view> comps;

        for (auto const& c: components(p)) {
            if (c == ".") {
                continue;
            }
            else if (c == "..") {
                if (empty(comps)) {
                    push_back(comps, c);
                }
                else {
                    auto&& last = comps.back();
                    if (last == "/") {
                        continue;
                    }
                    else if (last == "..") {
                        push_back(comps, c);
                    }
                    else {
                        pop_back(comps);
                    }
                }
            }
            else {
                push_back(comps, c);
            }
        }

        return join(comps, has_trailing_slash);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::path
