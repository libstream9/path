#include <stream9/path/basename.hpp>
#include <stream9/path/dirname.hpp>
#include <stream9/path/split.hpp>

#include <stream9/strings/find_last.hpp>

namespace stream9::path {

static void
chop_trailing_slash(string_view& p)
{
    std::size_t n = 0;
    auto i = p.end();
    auto b = p.begin();

    if (i == b) return;
    --i;

    while (i != b && *i == '/') {
        ++n;
        --i;
    }

    p.remove_suffix(n);
}

string_view
basename(string_view p)
{
    chop_trailing_slash(p);

    if (p.empty()) return ".";
    if (p == "/") return p;

    auto m = str::find_last(p, '/');
    if (m.empty()) {
        return p;
    }
    else {
        return { m.end(), p.end() };
    }
}

string_view
dirname(string_view p)
{
    chop_trailing_slash(p);

    if (p.empty()) return ".";
    if (p == "/") return p;

    auto [f, l] = str::find_last(p, '/');
    if (f == l) {
        return ".";
    }
    if (f == p.begin()) {
        return { f, f + 1 };
    }
    else {
        return { p.begin(), f };
    }
}

std::pair<string_view, string_view>
split(string_view p)
{
    chop_trailing_slash(p);

    if (p.empty()) return std::make_pair(".", ".");
    if (p == "/") return std::make_pair(p, p);

    auto [f, l] = str::find_last(p, '/');
    if (f == l) {
        return std::make_pair(".", p);
    }
    if (f == p.begin()) {
        auto i = f + 1;
        return std::make_pair(
            string_view(f, i),
            string_view(i, p.end())
        );
    }
    else {
        return std::make_pair(
            string_view(p.begin(), f),
            string_view(l, p.end())
        );
    }
}

} // namespace stream9::path
