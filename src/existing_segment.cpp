#include <stream9/path/existing_segment.hpp>

#include <stream9/cstring_ptr.hpp>
#include <stream9/path/dirname.hpp>

#include <sys/stat.h>

namespace stream9::path {

static bool
exists(string_view p)
{
    cstring_ptr path { p };
    struct ::stat st {};

    auto rc = ::stat(path.data(), &st);

    return rc != -1;
}

string_view
existing_segment(string_view p)
{
    if (p.empty()) {
        return ".";
    }

    if (exists(p)) {
        return p;
    }
    else {
        return existing_segment(dirname(p));
    }
}

} // namespace stream9::path
