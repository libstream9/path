#include <stream9/path/absolute.hpp>

#include <unistd.h>

#include <stream9/linux/error.hpp>
#include <stream9/path/concat.hpp>
#include <stream9/strings/empty.hpp>
#include <stream9/strings/starts_with.hpp>

namespace stream9::path {

static string
getcwd()
{
    auto rv = ::getcwd(nullptr, 0);
    if (rv == nullptr) {
        throw error {
            lx::make_error_code(errno)
        };
    }
    else {
        string cwd { rv };
        ::free(rv);

        return cwd;
    }
}

string
to_absolute(string_view p)
{
    try {
        using path::operator/;

        if (str::empty(p)) {
            return "/";
        }
        else if (str::starts_with(p, '/')) {
            return string(p);
        }
        else {
            return getcwd() / p;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::path
