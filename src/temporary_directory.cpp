#include <stream9/path/temporary_directory.hpp>

#include <stdlib.h>

#include <sys/stat.h>

namespace stream9::path {

static bool
directory_exists(cstring_view p) noexcept
{
    struct ::stat st {};

    auto rc = ::stat(p.data(), &st);
    if (rc == -1) {
        return false;
    }
    else {
        return S_ISDIR(st.st_mode);
    }
}

cstring_view
temporary_directory()
{
    try {
        if (char* p = ::getenv("TMPDIR")) {
            if (directory_exists(p)) {
                return p;
            }
        }

        if (directory_exists("/tmp")) {
            return "/tmp";
        }
        else if (directory_exists("/var/tmp")) {
            return "/var/tmp";
        }
        else {
            throw error { errc::directory_does_not_exist, };
        }
    }
    catch (...) { rethrow_error(); }
}

} // namespace stream9::path
