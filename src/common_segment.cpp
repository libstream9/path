#include <stream9/path/common_segment.hpp>

#include <stream9/path/components.hpp>

namespace stream9::path {

string_view
common_segment(string_view p1, string_view p2) noexcept
{
    components c1 { p1 };
    components c2 { p2 };

    auto first = p1.begin();
    auto last = p1.begin();

    auto i1 = c1.begin();
    auto e1 = c1.end();
    auto i2 = c2.begin();
    auto e2 = c2.end();

    for (; i1 != e1 && i2 != e2 && *i1 == *i2; ++i1, ++i2) {
        last = (*i1).end();
    }

    return { first, last };
}

} // namespace stream9::path
