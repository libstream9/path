#include <stream9/path/append.hpp>

#include <stream9/strings/back.hpp>
#include <stream9/strings/front.hpp>
#include <stream9/strings/substr.hpp>

namespace stream9::path {

string&
append(string& p1, string_view p2)
{
    try {
        if (p2.empty()) return p1;

        if (p1.empty()) {
            if (str::front(p2) == '/') {
                p1 = p2;
            }
            else {
                p1.append(p2);
            }
        }
        else if (str::back(p1) == '/') {
            if (str::front(p2) == '/') {
                p1.append(str::substr(p2, 1));
            }
            else {
                p1.append(p2);
            }
        }
        else {
            if (str::front(p2) == '/') {
                p1.append(p2);
            }
            else {
                p1.append('/');
                p1.append(p2);
            }
        }

        return p1;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::path
