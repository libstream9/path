#ifndef STREAM9_PATH_ERROR_HPP
#define STREAM9_PATH_ERROR_HPP

#include <system_error>
#include <source_location>

#include <stream9/errors.hpp>

namespace stream9::path {

enum class errc {
    ok = 0,
    directory_does_not_exist,
    invalid_environment_variable,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc e) noexcept
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::path

namespace std {

template<>
struct is_error_code_enum<stream9::path::errc> : true_type {};

} // namespace std

#endif // STREAM9_PATH_ERROR_HPP
