#ifndef STREAM9_PATH_APPEND_HPP
#define STREAM9_PATH_APPEND_HPP

#include "error.hpp"

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <utility>

namespace stream9::path {

string& append(string& p1, string_view p2);
string&& append(string&& p1, string_view p2);

string& operator/=(string& p1, string_view p2);
string&& operator/=(string&& p1, string_view p2);

/*
 * definition
 */
inline string&&
append(string&& p1, string_view p2)
{
    return std::move(append(p1, p2));
}

inline string&
operator/=(string& p1, string_view p2)
{
    return append(p1, p2);
}

inline string&&
operator/=(string&& p1, string_view p2)
{
    return append(std::move(p1), p2);
}

} // namespace stream9::path

#endif // STREAM9_PATH_APPEND_HPP
