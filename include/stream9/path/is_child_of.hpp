#ifndef STREAM9_PATH_IS_CHILD_OF_HPP
#define STREAM9_PATH_IS_CHILD_OF_HPP

#include "common_segment.hpp"

#include <stream9/strings/modifier/trim_right.hpp>

namespace stream9::path {

inline bool
is_child_of(string_view p1, string_view p2) noexcept
{
    str::trim_right(p2, '/');
    return common_segment(p1, p2) == p2;
}

} // namespace stream9::path

#endif // STREAM9_PATH_IS_CHILD_OF_HPP
