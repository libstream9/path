#ifndef STREAM9_PATH_COMPONENT_ITERATOR_HPP
#define STREAM9_PATH_COMPONENT_ITERATOR_HPP

#include <cassert>
#include <iterator>

#include <stream9/iterator_facade.hpp>
#include <stream9/string.hpp>

namespace stream9::path {

class component_iterator : public iterator_facade<component_iterator,
                                                  std::forward_iterator_tag,
                                                  string_view,
                                                  string_view >
{
public:
    using pointer = char const*;

public:
    component_iterator() = default; // partially-formed

    component_iterator(string_view s) noexcept
        : m_first { s.begin() }
        , m_end { s.end() }
    {
        if (m_first != m_end && *m_first == '/') {
            m_last = m_first + 1;
        }
        else {
            seek_last();
        }

        assert(m_first);
        assert(m_last);
        assert(m_end);
        assert(m_first <= m_last);
        assert(m_last <= m_end);
    }

private:
    friend class iterators::iterator_core_access;

    string_view
    dereference() const noexcept
    {
        assert(m_first);
        assert(m_last);
        assert(m_first <= m_last);

        return { m_first, m_last };
    }

    void
    increment() noexcept
    {
        assert(m_first);
        assert(m_first <= m_last);
        assert(m_last <= m_end);

        m_first = skip_slash(m_last);
        seek_last();
    }

    bool
    equal(component_iterator o) const noexcept
    {
        return m_first == o.m_first;
    }

    bool
    equal(std::default_sentinel_t) const noexcept
    {
        return m_first == m_end;
    }

    void
    seek_last() noexcept
    {
        auto p = m_first;
        while (p != m_end && *p != '/') {
            ++p;
        }
        m_last = p;
    }

    pointer
    skip_slash(pointer p) noexcept
    {
        while (*p == '/' && p != m_end) {
            ++p;
        }
        return p;
    }

private:
    pointer m_first {};
    pointer m_last {};
    pointer m_end {};
};

} // namespace stream9::path

#endif // STREAM9_PATH_COMPONENT_ITERATOR_HPP
