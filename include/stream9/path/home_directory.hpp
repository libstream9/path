#ifndef STREAM9_PATH_HOME_DIRECTORY_HPP
#define STREAM9_PATH_HOME_DIRECTORY_HPP

#include "error.hpp"

#include <stream9/string.hpp>

namespace stream9::path {

string
home_directory();

} // namespace stream9::path

#endif // STREAM9_PATH_HOME_DIRECTORY_HPP
