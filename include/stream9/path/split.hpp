#ifndef STREAM9_PATH_SPLIT_HPP
#define STREAM9_PATH_SPLIT_HPP

#include <utility>

#include <stream9/string_view.hpp>

namespace stream9::path {

/*
 * split path into dirname and basename
 *
 * = Example =
 * path        dirname basename
 * [empty]  -> .       .
 * /        -> /       /
 * ////     -> /       /
 * /usr/lib -> /usr    lib
 * /usr/    -> /       usr
 * usr      -> .       usr
 * /usr///  -> /       usr
 * .        -> .       .
 * ..       -> ..      ..
 * /usr/.   -> /usr    .
 * /usr/../ -> /usr    ..
 */
std::pair<string_view/*dirname*/, string_view/*basename*/>
split(string_view path);

} // namespace stream9::path

#endif // STREAM9_PATH_SPLIT_HPP
