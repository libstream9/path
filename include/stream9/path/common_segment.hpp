#ifndef STREAM9_PATH_COMMON_SEGMENT_HPP
#define STREAM9_PATH_COMMON_SEGMENT_HPP

#include <stream9/string.hpp>

namespace stream9::path {

/**
 * Return common part of path p1 and p2
 *
 * ex) commom_segment("/usr/local/bin", "/usr/share") == "/usr";
 */
string_view
common_segment(string_view p1, string_view p2) noexcept;

} // namespace stream9::path

#endif // STREAM9_PATH_COMMON_SEGMENT_HPP
