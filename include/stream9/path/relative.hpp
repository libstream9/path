#ifndef STREAM9_PATH_RELATIVE_HPP
#define STREAM9_PATH_RELATIVE_HPP

#include <stream9/string.hpp>

namespace stream9::path {

inline bool
is_relative(string_view p) noexcept
{
    return p.empty() || p[0] != '/';
}

} // namespace stream9::path

#endif // STREAM9_PATH_RELATIVE_HPP
