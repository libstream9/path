#ifndef STREAM9_PATH_EXISTING_SEGMENT_HPP
#define STREAM9_PATH_EXISTING_SEGMENT_HPP

#include <stream9/string.hpp>

namespace stream9::path {

string_view
existing_segment(string_view);

} // namespace stream9::path

#endif // STREAM9_PATH_EXISTING_SEGMENT_HPP
