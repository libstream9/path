#ifndef STREAM9_PATH_TEMPORARY_DIRECTORY_HPP
#define STREAM9_PATH_TEMPORARY_DIRECTORY_HPP

#include "error.hpp"

#include <stream9/cstring_view.hpp>

namespace stream9::path {

/*
 * Return path to the temporary directory
 * - value of environment variable TMPDIR
 * - /tmp
 * - /var/tmp
 *
 * It makes sure that the directory pointed by path exists.
 *
 * @throw errc::directory_does_not_exist
 */
cstring_view
temporary_directory();

} // namespace stream9::path

#endif // STREAM9_PATH_TEMPORARY_DIRECTORY_HPP
