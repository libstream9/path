#ifndef STREAM9_PATH_CONCAT_HPP
#define STREAM9_PATH_CONCAT_HPP

#include "append.hpp"

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

namespace stream9::path {

string concat(string_view p1, string_view p2);

string operator/(string_view p1, string_view p2);

/*
 * definition
 */
inline string
concat(string_view p1, string_view p2)
{
    return append(string(p1), p2);
}

inline string
operator/(string_view p1, string_view p2)
{
    return concat(p1, p2);
}

} // namespace stream9::path

#endif // STREAM9_PATH_CONCAT_HPP
