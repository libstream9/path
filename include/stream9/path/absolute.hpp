#ifndef STREAM9_PATH_ABSOLUTE_HPP
#define STREAM9_PATH_ABSOLUTE_HPP

#include <stream9/string.hpp>

#include <stream9/errors.hpp>

namespace stream9::path {

string
to_absolute(string_view p);

inline bool
is_absolute(string_view p) noexcept
{
    return !p.empty() && p[0] == '/';
}

} // namespace stream9::path

#endif // STREAM9_PATH_ABSOLUTE_HPP
