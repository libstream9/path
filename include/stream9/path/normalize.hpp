#ifndef STREAM9_PATH_NORMALIZE_HPP
#define STREAM9_PATH_NORMALIZE_HPP

#include "error.hpp"

#include <stream9/string.hpp>

namespace stream9::path {

string
normalize(string_view, bool preserve_trailing_slash = false);

} // namespace stream9::path

#endif // STREAM9_PATH_NORMALIZE_HPP
