#ifndef STREAM9_PATH_DIRNAME_HPP
#define STREAM9_PATH_DIRNAME_HPP

#include <stream9/string_view.hpp>

namespace stream9::path {

/*
 * extract dirname from path string
 *
 * = Example =
 * path        dirname
 * [empty]  -> .
 * /        -> /
 * ////     -> /
 * /usr/lib -> /usr
 * /usr/    -> /
 * usr      -> .
 * /usr///  -> /
 * .        -> .
 * ..       -> .
 * /usr/.   -> /usr
 * /usr/../ -> /usr
 */
string_view dirname(string_view path);

} // namespace stream9::path

#endif // STREAM9_PATH_DIRNAME_HPP
