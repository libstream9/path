#ifndef STREAM9_PATH_BASENAME_HPP
#define STREAM9_PATH_BASENAME_HPP

#include <stream9/string_view.hpp>

namespace stream9::path {

/*
 * extract filename from path string
 *
 * = Example =
 * path        basename
 * [empty]  -> .
 * /        -> /
 * ////     -> /
 * /usr/lib -> lib
 * /usr/    -> usr
 * usr      -> usr
 * /usr///  -> usr
 * .        -> .
 * ..       -> ..
 * /usr/.   -> .
 * /usr/../ -> ..
 */
string_view basename(string_view path);

} // namespace stream9::path

#endif // STREAM9_PATH_BASENAME_HPP
