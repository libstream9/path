#ifndef STREAM9_PATH_COMPONENTS_HPP
#define STREAM9_PATH_COMPONENTS_HPP

#include "component_iterator.hpp"

#include <iterator>

#include <stream9/json.hpp>
#include <stream9/string.hpp>

namespace stream9::path {

class components
{
public:
    components(string_view path) noexcept
        : m_path { path }
    {}

    component_iterator
    begin() const noexcept
    {
        return m_path;
    }

    std::default_sentinel_t
    end() const noexcept
    {
        return {};
    }

private:
    string_view m_path;
};

inline void
tag_invoke(json::value_from_tag, json::value& jv, components const& r)
{
    auto& arr = jv.emplace_array();

    for (auto const& c: r) {
        arr.push_back(c);
    }
}

} // namespace stream9::path

#endif // STREAM9_PATH_COMPONENTS_HPP
